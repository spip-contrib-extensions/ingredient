<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'ingredient_description' => '',
	'ingredient_nom' => 'ingrédients',
	'ingredient_slogan' => 'Gérer des liste d\'ingrédients dans vos articles SPIP !',
);

?>