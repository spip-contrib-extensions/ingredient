<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_ingredient' => 'Ajouter ce ingrédient',

	// C
	'champ_descriptif_label' => 'Descriptif',
	'champ_texte_label' => 'Texte',
	'champ_titre_label' => 'Titre',
	'quantite' => 'Quantité(e)',
	'quantite_confirme' => 'La quantitée a été mise à jour.',

	// I
	'icone_creer_ingredient' => 'Créer un ingrédient',
	'icone_modifier_ingredient' => 'Modifier ce ingrédient',
	'info_1_ingredient' => 'Un ingrédient',
	'info_aucun_ingredient' => 'Aucun ingrédient',
	'info_ingredients_auteur' => 'Les ingrédients de cet auteur',
	'info_nb_ingredients' => '@nb@ ingrédients',

	// R
	'retirer_lien_ingredient' => 'Retirer cet ingrédient',
	'retirer_tous_liens_ingredients' => 'Retirer tous les ingrédients',

	// T
	'texte_ajouter_ingredient' => 'Ajouter un ingrédient',
	'texte_changer_statut_ingredient' => 'Ce ingrédient est :',
	'texte_creer_associer_ingredient' => 'Créer et associer un ingrédient',
	'texte_definir_comme_traduction_ingredient' => 'Ce ingrédient est une traduction du ingrédient numéro :',
	'titre_ingredient' => 'Ingrédient',
	'titre_ingredients' => 'Ingrédients',
	'titre_ingredients_rubrique' => 'Ingrédients de la rubrique',
	'titre_langue_ingredient' => 'Langue de ce ingrédient',
	'titre_logo_ingredient' => 'Logo de ce ingrédient',

	'lien_article' => 'Liés à cet ingrédient :',

	'aucun_ingredient_article' => "Cet ingredient n'est pas utilisé.",
	'champ_titre_quantite' => 'Quantité',
	'message_ajoute_ok' => 'Ingredient corectement ajouté à la recette',
	'unite' => 'Unité de mesure',
	'unite_explication' => "Unité de mesure par défaut de l'ingrédient, elle sera automatiquement ajoutée au quantité des recettes.",
	'titre_liste_ingredients' => 'Les ingrédients de cette recette'
);
