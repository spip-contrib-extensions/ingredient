<?php
/**
 * Fonctions du squelette associé
 *
 * @plugin     ingrédients
 * @copyright  2015
 * @author     Phenix
 * @licence    GNU/GPL
 * @package    SPIP\Ingredient\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


// pour initiale et afficher_initiale
include_spip('prive/objets/liste/auteurs_fonctions');

?>